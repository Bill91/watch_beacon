package eu.beamdigital.watchbeacon;

import java.io.Serializable;

public class BeaconParam implements Serializable {
    // Device power
    public String battery;
    public String uuid;
    public String major;
    public String minor;
    // Calibration distance
    public String measurePower;
    // Broadcast power
    public String transmission;
    // Broadcast cycle
    public String broadcastingInterval;

    public String serialID;

    public String iBeaconMAC;

    public String iBeaconName;

    public String connectionMode;

    public BeaconDeviceInfo beaconInfo;

    public String password;

    public String threeAxis;

    @Override
    public String toString() {
        return "BeaconParam{" +
                "battery=" + battery +
                ", uuid='" + uuid + '\'' +
                ", major=" + major +
                ", minor=" + minor +
                ", measurePower='" + measurePower + '\'' +
                ", transmission=" + transmission +
                ", broadcastingInterval=" + broadcastingInterval +
                ", serialID='" + serialID + '\'' +
                ", iBeaconMAC='" + iBeaconMAC + '\'' +
                ", iBeaconName='" + iBeaconName + '\'' +
                ", connectionMode='" + connectionMode + '\'' +
                ", beaconInfo=" + beaconInfo.toString() +
                '}';
    }
}
