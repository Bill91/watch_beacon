package eu.beamdigital.watchbeacon;

import androidx.appcompat.app.AppCompatActivity;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothGattCharacteristic;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.animation.Animation;
import android.widget.Button;
import com.moko.support.MokoConstants;
import com.moko.support.MokoSupport;
import com.moko.support.callback.MokoOrderTaskCallback;
import com.moko.support.callback.MokoResponseCallback;
import com.moko.support.callback.MokoScanDeviceCallback;
import com.moko.support.entity.DeviceInfo;
import com.moko.support.entity.OrderType;
import com.moko.support.task.NotifyTask;
import com.moko.support.task.OrderTask;
import com.moko.support.task.ThreeAxesTask;
import com.moko.support.utils.MokoUtils;

import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;



public class MainActivity extends AppCompatActivity implements MokoScanDeviceCallback/*, MokoOrderTaskCallback, MokoResponseCallback*/ {

    private MokoService mMokoService;
    private BeaconParam mBeaconParam;
    private String mThreeAxis;
    private Animation animation = null;


    private HashMap<String, BeaconInfo> beaconMap = new HashMap<>();
    private BeaconInfoParseableImpl beaconInfoParseable = new BeaconInfoParseableImpl();
    private static final long SCAN_PERIOD = 6000;

    private Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        button = findViewById(R.id.infoBeacon);


        if (savedInstanceState != null) {

            MokoSupport.getInstance().disConnectBle();
            Log.d("TAG", "ciao");
            finish();
        }

        //todo: esiste un posto migliore per questa riga?
        MokoSupport.getInstance().init(getApplicationContext());

        //Richiedo i permessi
        PermissionWrapper pw = new PermissionWrapper();
        while (pw.check(this)) {
            pw.permission(this);
        }
        //Controllo BT
        if (!MokoSupport.getInstance().isBluetoothOpen()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, MokoConstants.REQUEST_CODE_ENABLE_BT);
            finish();
        }

        bindService(new Intent(this, MokoService.class), mServiceConnection, BIND_AUTO_CREATE);

    }


    private ServiceConnection mServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            mMokoService = ((MokoService.LocalBinder) service).getService();
            IntentFilter filter = new IntentFilter();
            filter.addAction(MokoConstants.ACTION_CONNECT_SUCCESS);
            filter.addAction(MokoConstants.ACTION_CONNECT_DISCONNECTED);
            filter.addAction(MokoConstants.ACTION_RESPONSE_SUCCESS);
            filter.addAction(MokoConstants.ACTION_RESPONSE_TIMEOUT);
            filter.addAction(MokoConstants.ACTION_RESPONSE_FINISH);
            filter.addAction(BluetoothAdapter.ACTION_STATE_CHANGED);
            filter.addAction(MokoConstants.ACTION_RESPONSE_NOTIFY);
            //todo metti le action che mancano per far funzionare il broadcast receiver
            filter.setPriority(100);
            registerReceiver(mReceiver, filter);
            if (animation == null) {

                startScan();
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
        }
    };

    private BroadcastReceiver mReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null) {
                String action = intent.getAction();
                if (MokoConstants.ACTION_CONNECT_SUCCESS.equals(action)) {
                    mBeaconParam = new BeaconParam();
                    BeaconDeviceInfo beaconInfo = new BeaconDeviceInfo();
                    mBeaconParam.beaconInfo = beaconInfo;
                    mBeaconParam.threeAxis = mThreeAxis;
                    // Legge tutti i dati leggibili
                    mMokoService.mHandler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mBeaconParam.password = "Moko4321";
                            mMokoService.getReadableData("Moko4321");
                        }
                    }, 1000);
                }
                if (MokoConstants.ACTION_CONNECT_DISCONNECTED.equals(action)) {
                    //dismissLoadingProgressDialog();
                    //ToastUtils.showToast(MainActivity.this, "connect failed");
                    if (animation == null) {
                        startScan();
                    }
                }
                if (MokoConstants.ACTION_RESPONSE_TIMEOUT.equals(action)) {
                    OrderType orderType = (OrderType) intent.getSerializableExtra(MokoConstants.EXTRA_KEY_RESPONSE_ORDER_TYPE);
                    switch (orderType) {
                        case changePassword:
                            // change password timeout
                            //dismissLoadingProgressDialog();
                            //ToastUtils.showToast(MainActivity.this, "password error");
                            if (animation == null) {
                                startScan();
                            }
                            break;
                    }
                }
                if (MokoConstants.ACTION_RESPONSE_FINISH.equals(action)) {
                }
                if (MokoConstants.ACTION_RESPONSE_SUCCESS.equals(action)) {
                    OrderType orderType = (OrderType) intent.getSerializableExtra(MokoConstants.EXTRA_KEY_RESPONSE_ORDER_TYPE);
                    byte[] value = intent.getByteArrayExtra(MokoConstants.EXTRA_KEY_RESPONSE_VALUE);
                    switch (orderType) {
                        case battery:
                            mBeaconParam.battery = Integer.parseInt(MokoUtils.bytesToHexString(value), 16) + "";
                            break;
                        case iBeaconUuid:
                            String hexString = MokoUtils.bytesToHexString(value).toUpperCase();
                            if (hexString.length() > 31) {
                                StringBuilder sb = new StringBuilder();
                                sb.append(hexString.substring(0, 8));
                                sb.append("-");
                                sb.append(hexString.substring(8, 12));
                                sb.append("-");
                                sb.append(hexString.substring(12, 16));
                                sb.append("-");
                                sb.append(hexString.substring(16, 20));
                                sb.append("-");
                                sb.append(hexString.substring(20, 32));
                                String uuid = sb.toString();
                                mBeaconParam.uuid = uuid;
                            }
                            break;
                        case major:
                            mBeaconParam.major = Integer.parseInt(MokoUtils.bytesToHexString(value), 16) + "";
                            break;
                        case minor:
                            mBeaconParam.minor = Integer.parseInt(MokoUtils.bytesToHexString(value), 16) + "";
                            break;
                        case measurePower:
                            mBeaconParam.measurePower = Integer.parseInt(MokoUtils.bytesToHexString(value), 16) + "";
                            break;
                        case transmission:
                            int transmission = Integer.parseInt(MokoUtils.bytesToHexString(value), 16);
                            if (transmission == 8) {
                                transmission = 7;
                            }
                            mBeaconParam.transmission = transmission + "";
                            break;
                        case broadcastingInterval:
                            mBeaconParam.broadcastingInterval = Integer.parseInt(MokoUtils.bytesToHexString(value), 16) + "";
                            break;
                        case serialID:
                            mBeaconParam.serialID = MokoUtils.hex2String(MokoUtils.bytesToHexString(value));
                            break;
                        case iBeaconMac:
                            String hexMac = MokoUtils.bytesToHexString(value);
                            if (hexMac.length() > 11) {
                                StringBuilder sb = new StringBuilder();
                                sb.append(hexMac.substring(0, 2));
                                sb.append(":");
                                sb.append(hexMac.substring(2, 4));
                                sb.append(":");
                                sb.append(hexMac.substring(4, 6));
                                sb.append(":");
                                sb.append(hexMac.substring(6, 8));
                                sb.append(":");
                                sb.append(hexMac.substring(8, 10));
                                sb.append(":");
                                sb.append(hexMac.substring(10, 12));
                                String mac = sb.toString().toUpperCase();
                                mBeaconParam.iBeaconMAC = mac;
                                mBeaconParam.beaconInfo.iBeaconMac = mac;
                            }
                            break;
                        case iBeaconName:
                            mBeaconParam.iBeaconName = MokoUtils.hex2String(MokoUtils.bytesToHexString(value));
                            break;
                        case connectionMode:
                            mBeaconParam.connectionMode = MokoUtils.bytesToHexString(value);
                            break;
                        case firmname:
                            mBeaconParam.beaconInfo.firmname = MokoUtils.hex2String(MokoUtils.bytesToHexString(value));
                            break;
                        case softVersion:
                            mBeaconParam.beaconInfo.softVersion = MokoUtils.hex2String(MokoUtils.bytesToHexString(value));
                            break;
                        case devicename:
                            mBeaconParam.beaconInfo.deviceName = MokoUtils.hex2String(MokoUtils.bytesToHexString(value));
                            break;
                        case iBeaconDate:
                            mBeaconParam.beaconInfo.iBeaconDate = MokoUtils.hex2String(MokoUtils.bytesToHexString(value));
                            break;
                        case hardwareVersion:
                            mBeaconParam.beaconInfo.hardwareVersion = MokoUtils.hex2String(MokoUtils.bytesToHexString(value));
                            break;
                        case firmwareVersion:
                            mBeaconParam.beaconInfo.firmwareVersion = MokoUtils.hex2String(MokoUtils.bytesToHexString(value));
                            break;
                        case writeAndNotify:
                            if ("eb59".equals(MokoUtils.bytesToHexString(Arrays.copyOfRange(value, 0, 2)).toLowerCase())) {
                                byte[] runtimeBytes = Arrays.copyOfRange(value, 4, value.length);
                                int seconds = Integer.parseInt(MokoUtils.bytesToHexString(runtimeBytes), 16);
                                int day = 0, hours = 0, minutes = 0;
                                day = seconds / (60 * 60 * 24);
                                seconds -= day * 60 * 60 * 24;
                                hours = seconds / (60 * 60);
                                seconds -= hours * 60 * 60;
                                minutes = seconds / 60;
                                seconds -= minutes * 60;
                                mBeaconParam.beaconInfo.runtime = String.format("%dD%dh%dm%ds", day, hours, minutes, seconds);
                            }
                            if ("eb5b".equals(MokoUtils.bytesToHexString(Arrays.copyOfRange(value, 0, 2)).toLowerCase())) {
                                byte[] chipModelBytes = Arrays.copyOfRange(value, 4, value.length);
                                mBeaconParam.beaconInfo.chipModel = MokoUtils.hex2String(MokoUtils.bytesToHexString(chipModelBytes));
                            }
                            break;
                        case changePassword:
                            if ("00".equals(MokoUtils.bytesToHexString(value))) {
                                Log.d("TAG", "ok");
                                //todo funziona sia con la prossima riga(oppure quella dopo), che con quello messo in getReadableData, adesso è commentato anche quello
                                mMokoService.sendOrder(mMokoService.setNotify(),mMokoService.setThreeAxes(true));
                                // todo a questo punto io mi toglierei sicuro i metodi notifiche e accelerometro e forse forse anche i callback
                                mMokoService.mHandler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        //dismissLoadingProgressDialog();
                                        //LogModule.i(mBeaconParam.toString());
                                        //mSavedPassword = mPassword;
/*                                        Intent deviceInfoIntent = new Intent(MainActivity.this, DeviceInfoActivity.class);
                                        deviceInfoIntent.putExtra(BeaconConstants.EXTRA_KEY_DEVICE_PARAM, mBeaconParam);
                                        startActivityForResult(deviceInfoIntent, BeaconConstants.REQUEST_CODE_DEVICE_INFO);*/
                                    }
                                }, 5000);
                            } else {
                                //dismissLoadingProgressDialog();
                                //ToastUtils.showToast(MainActivity.this, "password error");
                                if (animation == null) {
                                    startScan();
                                }
                            }
                            break;
                    }
                }
                if (BluetoothAdapter.ACTION_STATE_CHANGED.equals(action)) {
                    int blueState = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, 0);
                    switch (blueState) {
                        case BluetoothAdapter.STATE_TURNING_OFF:
                            if (animation != null) {
                                mMokoService.mHandler.removeMessages(0);
                                mMokoService.stopScanDevice();
                            }
                            break;

                    }
                }
                if (MokoConstants.ACTION_RESPONSE_NOTIFY.equals(action)) {
                    Log.d("TAG", "SEZIONE UTILE MainActivity");
                    OrderType orderType = (OrderType) intent.getSerializableExtra(MokoConstants.EXTRA_KEY_RESPONSE_ORDER_TYPE);
                    byte[] value = intent.getByteArrayExtra(MokoConstants.EXTRA_KEY_RESPONSE_VALUE);
                    switch (orderType) {
                        case writeAndNotify:
                            Log.d("TAG", "CASE UTILE MainActivity");
                            String threeAxisStr = MokoUtils.bytesToHexString(value);
                            if (threeAxisStr.length() >= 20) {
                                //builder.append(String.format("----<X：%s；Y：%s；Z：%s>", threeAxisStr.substring(8, 12), threeAxisStr.substring(12, 16), threeAxisStr.substring(16, 20)));
                                //builder.append("\n");
//                                X.setText("X: "+threeAxisStr.substring(8, 12));
//                                Y.setText("Y: "+threeAxisStr.substring(12, 16));
//                                Z.setText("Z: "+threeAxisStr.substring(16, 20));
                                button.setText("Nome: " +mBeaconParam.iBeaconName+ "\n" + "MAC: " + mBeaconParam.iBeaconMAC+ "\n" + "X: " +threeAxisStr.substring(8, 12)+ "; Y: " + threeAxisStr.substring(8, 12) + "; Z: " + threeAxisStr.substring(8, 12));

                                Log.d("TAG", "----<X：" + threeAxisStr.substring(8, 12) + "；Y：" + threeAxisStr.substring(8, 12) + "；Z：" + threeAxisStr.substring(8, 12));
//                                tvDeviceThreeAxis.setText(builder.toString());
//                                scrollView.post(new Runnable() {
//                                    @Override
//                                    public void run() {
//                                        scrollView.fullScroll(ScrollView.FOCUS_DOWN);
//                                    }
//                                });
                            }
                            break;
                    }
                }

            }

        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case MokoConstants.REQUEST_CODE_ENABLE_BT:
                    if (animation == null) {
                        startScan();
                    }
                    break;

            }
        } else {
            switch (requestCode) {
                case MokoConstants.REQUEST_CODE_ENABLE_BT:
                    // BT è spento
                    MainActivity.this.finish();
                    break;
                case BeaconConstants.REQUEST_CODE_DEVICE_INFO:
                    if (animation == null) {
                        startScan();
                    }
                    break;
            }
        }
    }

    private void startScan() {
/*        if (!MokoSupport.getInstance().isBluetoothOpen()) {
            // 蓝牙未打开，开启蓝牙
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, MokoConstants.REQUEST_CODE_ENABLE_BT);
            return;
        }*/
/*        animation = AnimationUtils.loadAnimation(this, R.anim.rotate_refresh);
        ivRefresh.startAnimation(animation);
        beaconInfoParseable = new BeaconInfoParseableImpl();
        if (!isLocationPermissionOpen()) {
            return;
        }*/
        mMokoService.startScanDevice(this);
        mMokoService.mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                mMokoService.stopScanDevice();
            }
        }, SCAN_PERIOD /*1000 * 60*/);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d("TAG", "destroy");
        unregisterReceiver(mReceiver);
        unbindService(mServiceConnection);

    }

    @Override
    public void onStartScan() {
        Log.d("TAG", "startscan");
        beaconMap.clear();

    }

    @Override
    public void onScanDevice(DeviceInfo device) {
        if (device.mac.equals("FF:01:13:D9:42:0A")) {

            if (!(beaconMap.containsKey(device.mac))) {
                BeaconInfo beaconInfo = beaconInfoParseable.parseDeviceInfo(device);
                beaconMap.put(beaconInfo.mac, beaconInfo);
                button.setText("Nome: " + device.name + "\n" + "MAC: " + device.mac + "\n" + "X: " + beaconInfo.threeAxis.substring(0, 4) + "; Y: " + beaconInfo.threeAxis.substring(4, 8) + "; Z: " + beaconInfo.threeAxis.substring(8, 12));
            }

        }


/*            BeaconInfo beaconInfo = beaconInfoParseable.parseDeviceInfo(device);
        if (beaconInfo == null) {
            return;
        }
        beaconMap.put(beaconInfo.mac, beaconInfo);*/

    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d("TAG", "RESTART RESTART RESTART");

    }


    @Override
    public void onStopScan() {
        Log.d("TAG", "stopScan");
        //todo utilizza l'interno del for nel click button per gestire la connessione a seguito del click
        for (String a : beaconMap.keySet()) {
            //mMokoService.connDevice(a);
            Log.d("TAG", "connetto a " + a);
            mMokoService.connDevice(a);
            //todo quando lui si collega chiama tutta una serie di task, che secondo me possiamo anche far sparire

        }

        //todo: ricordati di fallo sparire
        //MokoSupport.getInstance().disConnectBle();
        animation = null;
    }

}
