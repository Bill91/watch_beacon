package eu.beamdigital.watchbeacon;

import java.io.Serializable;

public class BeaconDeviceInfo implements Serializable {
    public String softVersion;
    // manufacturer
    public String firmname;
    // product number
    public String deviceName;
    // product date
    public String iBeaconDate;

    public String iBeaconMac;

    public String chipModel;

    public String hardwareVersion;

    public String firmwareVersion;
    // operation hours
    public String runtime;

    @Override
    public String toString() {
        return "BeaconDeviceInfo{" +
                "softVersion='" + softVersion + '\'' +
                ", firmname='" + firmname + '\'' +
                ", deviceName='" + deviceName + '\'' +
                ", iBeaconDate='" + iBeaconDate + '\'' +
                ", iBeaconMac='" + iBeaconMac + '\'' +
                ", chipModel='" + chipModel + '\'' +
                ", hardwareVersion='" + hardwareVersion + '\'' +
                ", firmwareVersion='" + firmwareVersion + '\'' +
                ", runtime='" + runtime + '\'' +
                '}';
    }
}
